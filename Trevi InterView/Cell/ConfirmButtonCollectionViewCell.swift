//
//  ConfirmButtonCollectionViewCell.swift
//  Trevi InterView
//
//  Created by 林翌埕 on 2018/8/22.
//  Copyright © 2018年 YochaStudio. All rights reserved.
//

import UIKit

class ConfirmButtonCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var uiViewBackGround: UIView!
    @IBOutlet weak var uiBtnConfirm: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
