//
//  DataInputViewController.swift
//  Trevi InterView
//
//  Created by 林翌埕 on 2018/8/22.
//  Copyright © 2018年 YochaStudio. All rights reserved.
//

import UIKit

class DataInputViewController: UIViewController {
    
    /// UIElements
    ///
    @IBOutlet weak var uiTxtFieldColumn: UITextField!
    @IBOutlet weak var uiTxtFieldRow: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func pressedConfirmButton() {
        performSegue(Segues.segueToGridLayoutViewController, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let GridLayoutVC = segue.destination as! GridLayoutViewController
        GridLayoutVC.Column = UInt(uiTxtFieldColumn.text!)
        GridLayoutVC.Row    = UInt(uiTxtFieldRow.text!)
    }
}


/// Segues enum
///
/// - segueToGridLayoutViewController
///
extension DataInputViewController {
    internal enum Segues: String, Segue {
        case segueToGridLayoutViewController
    }
}
