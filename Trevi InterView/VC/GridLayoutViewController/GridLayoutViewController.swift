//
//  GridLayoutViewController.swift
//  Trevi InterView
//
//  Created by 林翌埕 on 2018/8/22.
//  Copyright © 2018年 YochaStudio. All rights reserved.
//

import UIKit

class GridLayoutViewController: UIViewController {
    
    /// User inputed these two numbers to determine the grid layout.
    ///
    var Column: UInt!
    var Row:    UInt!
    var RandomColumn: UInt32!
    var RandomRow:    UInt32!
    
    /// Timer Properties
    ///
    ///  - CountDownTimer
    ///  - TimerSeconds:    The seconds for the countdown timer.
    ///  - RemainingSeconds
    ///  - isTimerRunning:  Status of Timer. Prevent multiple Timers running at the same time.
    ///
    var CountDownTimer = Timer()
    let TimerSeconds:TimeInterval = 10
    var RemainingSeconds:TimeInterval!
    var isTimerRunning = false
    
    /// The Grid Layout CollectionView
    ///
    @IBOutlet weak var uiCollectionViewGridLayout: UICollectionView!
    
    var uiViewHighlightView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Column: \(Column!), Row: \(Row!)")
        configure(collectionView: uiCollectionViewGridLayout)
        startRandom()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopTimer()
    }
}

 
