//
//  GridLayoutViewController+Additions.swift
//  Trevi InterView
//
//  Created by 林翌埕 on 2018/8/22.
//  Copyright © 2018年 YochaStudio. All rights reserved.
//

import UIKit

extension GridLayoutViewController {
    
    /// Grid Layout Collection View configuration
    ///
    internal func configure(collectionView: UICollectionView) {
        collectionView.register(cellType: NormalCardCollectionViewCell.self)
        collectionView.register(cellType: ConfirmButtonCollectionViewCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    internal func startRandom() {
        randomColumnAndRow()
        applyHighlightBorder()
        startTimer()
    }
    
    private func randomColumnAndRow() {
        RandomColumn = arc4random_uniform(UInt32(Column))
        RandomRow = arc4random_uniform(UInt32(Row))
        print("RandomColumn: \(RandomColumn!), RandomRow: \(RandomRow!)")
        uiCollectionViewGridLayout.reloadData()
    }
    
    private func applyHighlightBorder() {
        let CollectionViewGridLayoutWidth = uiCollectionViewGridLayout.frame.width
        let CollectionViewGridLayoutHeight = uiCollectionViewGridLayout.frame.height
        let RandomCell = uiCollectionViewGridLayout.dequeueReusableCell(for: IndexPath(row: Int(RandomRow), section: Int(RandomColumn))) as NormalCardCollectionViewCell
        
        if uiViewHighlightView != nil {
            uiViewHighlightView.removeFromSuperview()
        }
        
        uiViewHighlightView = UIView(frame: CGRect(x: RandomCell.frame.origin.x - 1, y: uiCollectionViewGridLayout.frame.origin.y, width: RandomCell.frame.width + 2, height: CollectionViewGridLayoutHeight))
        uiViewHighlightView.backgroundColor = UIColor.clear
        uiViewHighlightView.layer.borderWidth = 2
        uiViewHighlightView.layer.borderColor = UIColor.red.cgColor
        self.view.addSubview(uiViewHighlightView)
    }
}


/// MARK: - UICollectionViewDelegate
///
extension GridLayoutViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Int(Column)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(Row) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.row {
            
        /// Add Confirm Button Cells in the last row
        ///
        case Int(Row):
            let cell = collectionView.dequeueReusableCell(for: indexPath) as ConfirmButtonCollectionViewCell
            
            /// determine the cell is highlight or not.
            ///
            ///  - better to separate this judgement to applyHighlightBorder method
            ///
            cell.uiViewBackGround.backgroundColor = UIColor.lightGray
            if indexPath.section == Int(RandomColumn) {
                cell.uiViewBackGround.backgroundColor = UIColor.red
            }
            
            cell.uiBtnConfirm.layer.borderColor = UIColor.white.cgColor
            cell.uiBtnConfirm.layer.borderWidth = 2
            return cell
            
        /// Default to return a NormalCard cell
        //
        default:
            let cell = collectionView.dequeueReusableCell(for: indexPath) as NormalCardCollectionViewCell
            cell.uiViewBackGround.layer.borderWidth = 1
            cell.uiViewBackGround.layer.borderColor = UIColor.black.cgColor
            cell.uiLblRandom.isHidden = true
            if indexPath == IndexPath(row: Int(RandomRow), section: Int(RandomColumn)) {
                cell.uiLblRandom.isHidden = false
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/CGFloat(Column) - 4, height: collectionView.frame.height/CGFloat(Row+1) - 4)
    }
}


/// MARK: - Timer
///
extension GridLayoutViewController {
    
    /// start countdown Timer
    ///
    /// Scheduled a timer if Timer isn't running yet and set the running status to true
    /// or just reset the remaining seconds.
    ///
    private func startTimer() {
        if !isTimerRunning {
            CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            isTimerRunning = true
        }
        RemainingSeconds = TimerSeconds
    }
    
    /// update countdown timer
    ///
    /// minus 1 second and check if there is no remaining seconds.
    /// and then random a new card.
    ///
    @objc private func updateTimer() {
        RemainingSeconds = RemainingSeconds - 1
        if RemainingSeconds <= 0 {
            startRandom()
        }
        print("Remaining \(RemainingSeconds!) Seconds")
    }
    
    /// stop countdown timer
    ///
    /// invalidate the timer and set running status to false
    ///
    internal func stopTimer() {
        CountDownTimer.invalidate()
        isTimerRunning = false
    }
}
